window.onload = function() {
  //<editor-fold desc="Changeable Configuration Block">

  // the following lines will be replaced by docker/configurator, when it runs in a docker-container
  window.ui = SwaggerUIBundle({
    url: "https://ucy-itas.bitbucket.io/api/openapi.json",
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  });
  window.ui.initOAuth({
    clientId: "5e854adb-4f4f-4cb5-89ad-4143d027a30b",
    scopeSeparator: " ",
    scopes: "openid profile",
    usePkceWithAuthorizationCodeGrant: true
  });
  //</editor-fold>
};
